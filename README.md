# vue3-svg-map

Prototype integration of a SVG map in a vuejs 3 component.

Homepage: https://gitlab.com/simevo/vue3-svg-map

Based on an idea by Anamol Soman: https://medium.com/js-dojo/how-to-integrate-interactive-svg-map-in-vuejs-fafc067baf5f

Screen-shot:

![screen-shot](./images/screenshot.png "Map as shown in the app")

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
yarnpkg
```

### Compile and Hot-Reload for Development

```sh
yarnpkg dev
```

### Compile and Minify for Production

```sh
yarnpkg build
```

### Lint with [ESLint](https://eslint.org/)

```sh
yarnpkg lint
```
